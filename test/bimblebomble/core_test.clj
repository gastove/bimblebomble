(ns bimblebomble.core-test
  (:require [bimblebomble.core :refer :all]
            [cheshire.core :as json]
            [clojure.test :refer :all]
            [clojure.java.io :as io]))

(deftest test-line-parsing
  (testing "Can we correctly clean a phone number?"
    (is (= "+1234567890" (clean-phone-number "+1234567890"))
        "An E.164 phone number should be returned verbatim.")
    (is (= "+11234567890" (clean-phone-number "(123) 456-7890"))
        "A phone number in (xxx) xxx-xxxx format should be parsed to E.164 format"))
  (testing "Can we correctly clean a line?"
    (let [test-lines ["+13058224036,zendesk,Nowlin Saul"
                      "(609) 491-4267,home,Rubino Lennoxlove"]
          expected [(->CallerIDRecord "+13058224036" "zendesk" "Nowlin Saul")
                    (->CallerIDRecord "+16094914267" "home" "Rubino Lennoxlove")]
          result (map parse-line test-lines)]
      (is (= expected result) "We should get correctly parsed lines."))))

(deftest test-data-functions
  ;; Test rowz
  (let [test-row-one   (->CallerIDRecord "+13058224036" "zendesk" "Nowlin Saul")
        test-row-two   (->CallerIDRecord "+16094914267" "home" "Rubino Lennoxlove")
        ;; new number/context, same name
        test-row-three (->CallerIDRecord "+13051859875" "plab" "Nowlin Saul")
        ;; Duplicated number/context, different name
        test-row-four  (->CallerIDRecord "+13058224036" "zendesk" "Zaphod Beeblebrox")
        test-rows      [test-row-one test-row-two test-row-three test-row-four]
        state          {}
        store          (atom {})]

    (testing "Can we upsert a row?"
      (let [expected {"+13058224036" {"zendesk" ["Nowlin Saul"]}}]
        (is (= (upsert state test-row-one) expected)
            "Upserting new rows should correctly update or insert to storage"))

      (let [expected {"+13058224036" {"zendesk" ["Nowlin Saul" "Zaphod Beeblebrox"]}}
            state    (upsert state test-row-one)]
        (is (= (upsert state test-row-four) expected)
            "New names should append if phone number/context are the same")))

    (testing "Can we correctly build our lookup structure?"
      (let [expected {"+13051859875" {"plab" ["Nowlin Saul"]}
                      "+16094914267" {"home" ["Rubino Lennoxlove"]}
                      "+13058224036" {"zendesk" ["Nowlin Saul" "Zaphod Beeblebrox"]}}]
        (build-lookup test-rows store)
        (is (=  @store expected))))

    (testing "Can we do lookups on the lookup structure?"
      (is (= (lookup-caller-by-pn "+13051859875" store) {"plab" ["Nowlin Saul"]}))
      (is (= (lookup-caller-by-pn "+13058224036" store) {"zendesk" ["Nowlin Saul" "Zaphod Beeblebrox"]})))
    (testing "Can we correctly format the return value for a lookup?"
      (let [first-number "+16094914267"
            first-number-return {"home" ["Rubino Lennoxlove"]}
            first-number-expected [(->CallerIDRecord "+16094914267" "home" "Rubino Lennoxlove")]
            second-number "+13058224036"
            second-number-return {"zendesk" ["Nowlin Saul" "Zaphod Beeblebrox"]}
            second-number-expected [(->CallerIDRecord "+13058224036" "zendesk" "Nowlin Saul")
                                    (->CallerIDRecord  "+13058224036" "zendesk" "Zaphod Beeblebrox")]
            first-actual (format-lookup-return first-number first-number-return)
            second-actual (format-lookup-return second-number second-number-return)]
        (is (= first-number-expected first-actual))
        (is (= second-number-expected second-actual))))
    ))

(deftest test-service-functions
  (testing "Can we correctly assemble our root URI?"
    (reset! service {:opts {:port 12345}})
    (is (= "http://0.0.0.0:12345" (get-root-uri)))
    (reset! service nil))

  (testing "Can we parse invalid JSON without throwing?"
    (let [good-json "{\"cat\": 5}"
          bad-json  "{\"cat\": }"]
      (is (safe-parse-json good-json))
      (is (not (safe-parse-json bad-json)))))

  (testing "However humbly, can we validate a POST body?"
    (let [good-post           {:name "Zaphod Beeblebrox" :context "improable" :number "+11234567890"}
          bad-post-name-empty {:name "" :context "improable" :number "+11234567890"}
          bad-post-ctx-empty  {:name "Zaphod Beeblebrox" :context "" :number "+11234567890"}
          bad-post-bad-pn     {:name "Zaphod Beeblebrox" :context "improable" :number "8675309"}]
      (is (validate-parsed-json good-post))
      (is (not (validate-parsed-json bad-post-name-empty)))
      (is (not (validate-parsed-json bad-post-ctx-empty)))
      (is (not (validate-parsed-json bad-post-bad-pn)))))

  (testing "Do we get correct responses on attempts to POST both valid and invalid JSON bodies?"
    ;; Service URI is in the header, so this needs to be set before accessing it
    (reset! service {:opts {:port 12345}})

    (let [good-post          {:name "Zaphod Beeblebrox" :context "improbable" :number "+11234567890"}
          good-post-json     (json/generate-string good-post)
          good-post-stream   (java.io.StringReader. good-post-json)
          good-post-expected {:status  201
                              :headers {"Content-Type" "text/plain"
                                        "Location"     "http://0.0.0.0:12345/query?number=%2B11234567890"}
                              :body    good-post-json}
          bad-post           {:name "" :context "improable" :number "+11234567890"}
          bad-post-json      (json/generate-string bad-post)
          bad-post-stream    (java.io.StringReader. bad-post-json)
          bad-post-expected  {:status  400
                              :headers {"Content-Type" "text/plain"}
                              :body    "Malformed JSON in POST body"}
          store              (atom {})]
      (is (= good-post-expected (post-new-number good-post-stream store)))
      (is (= (@store "+11234567890") {"improbable" ["Zaphod Beeblebrox"]}))
      (is (= bad-post-expected (post-new-number bad-post-stream store)))
      )))
