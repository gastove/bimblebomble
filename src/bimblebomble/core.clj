(ns bimblebomble.core
  (:gen-class)
  (:require [cheshire.core :as json]
            [clojure.java.io :as io]
            [clojure.string :as str]
            [clojure.tools.cli :refer [parse-opts]]
            [compojure.core :refer [ANY defroutes GET POST]]
            [org.httpkit.server :as http-kit]
            [ring.middleware.defaults :refer [api-defaults wrap-defaults]]
            [ring.middleware.reload :refer [wrap-reload]]
            [taoensso.timbre :as log])
  (:import java.net.URLEncoder))

;; Who doesn't love a string constant?
(def seed-file-name "interview-callerid-data.csv")

;;---------------------CSV Loading/Parsing, Data cleaning-----------------------

(defrecord CallerIDRecord [number context name])

(def wrong-pn-format-re #"\((\d{3})\) (\d{3})-(\d{4})")

(defn clean-phone-number [pn]
  "Phone numbers in the given CSV come in two formats: E.164, and the wrong
  one. Fortunately, every E.164 number begins with +."
  (if (not= (first pn) \+)
    (->> (re-matches wrong-pn-format-re pn)
         rest
         (cons "+1")
         str/join)
    pn))

(defn parse-line [line]
  "Given a line from a CSV, split on comma, clean the phone number, and create a
  CallerIDRecord"
  (let [[raw-pn ctx caller] (str/split line #",")
        cleaned-pn          (clean-phone-number raw-pn)]
    (->CallerIDRecord cleaned-pn ctx caller)))

(defn load-and-parse-csv [file-name]
  "Does what it says on the tin -- read a given resource `file-name`, parse it."
  (let [contents (-> file-name
                     io/resource
                     io/reader
                     line-seq)]
    (log/debug "Loading" file-name)
    (map parse-line contents)
    (log/debug "Loaded" (count contents) "lines of seed data")))

;;-----------------------Data storage, lookup, updating-------------------------

(defn upsert [state cid-record]
  "Given a CallerIDRecord and a store, append the caller name appropriately and
  return an updated store."
  (let [key           [(:number cid-record) (:context cid-record)]
        current-state (or (get-in state key) [])
        new-state     (conj current-state (:name cid-record))]
    (assoc-in state key new-state)))

(defn build-lookup [seed-data store]
  "Ingest a vector of CallerIDRecords; generate a lookup structure for building
  API returns."
  (doseq [cid-record seed-data] (swap! store upsert cid-record)))

(defn lookup-caller-by-pn [phone-number store]
  "Return a record for a given phone number, or nil if not found."
  (@store phone-number))

;;--------------------------------Serv[er/ice]----------------------------------
(def application-storage (atom {}))
(def service (atom nil))

;; Root URI
(defn get-root-uri []
  ;; Eventually, much of this might be configurable; isn't right now.
  (let [scheme "http"
        ip     "0.0.0.0"
        port   (get-in @service [:opts :port])]
    (str scheme "://" ip ":" port)))

;; Responses
;; Anything I don't have tidy utilities for already
(defn url-encode [s]
  (URLEncoder/encode s "utf8"))

(defn respond-http-post-success [body loc]
  {:status  201
   :headers {"Content-Type" "text/plain"
             "Location"     loc}
   :body    body})

(defn respond-with-json [body]
  (let [json-body (json/generate-string body)]
    {:status 200
     :headers {"Content-Type" "application/json"}
     :body json-body}))

(defn four-hundred
  ([message] (four-hundred message 404))
  ([message status]
   {:status status
    :headers {"Content-Type" "text/plain"}
    :body message}))

(defn format-lookup-return [pn return]
  "Given a phone number and the lookup result for it -- a map of
  {context [names]} -- create nicely formatted maps for JSON-encoded response
  bodies."
  (-> (for [[context cid-names] return]
        (map (fn [n] (map->CallerIDRecord {:number pn :context context :name n})) cid-names))
      flatten
      vec))

(defn query [phone-number store]
  "Try to look up a phone number in the given store; return a JSON response on
  success and a 404 with an appropriate message on failure."
  (log/debug  "Querying for:" phone-number)
  (if-let [lookup-return (lookup-caller-by-pn phone-number store)]
    (respond-with-json (format-lookup-return phone-number lookup-return))
    (four-hundred (str "No record found for number: " phone-number))))

(defn safe-parse-json [json-string]
  "Parse JSON inside a try so we don't explode if its invalid."
  (try (json/parse-string json-string true)
       (catch com.fasterxml.jackson.core.JsonParseException jpe nil)))

(defn validate-parsed-json [body]
  "There's very little to do right now validating the context and name fields of
  a POST body, but I can, at least:
  1. Be sure the phone number is in E.164 format
  2. Neither context nor name are empty"
  (and (not (str/blank? (:name body)))
       (not (str/blank? (:context body)))
       ;; Short-circuit so we don't NPE on a blank phone number regex check
       (not (str/blank? (:number body)))
       (re-matches #"\+1\d{10}" (:number body))))

(defn safe-parse-and-validate-post-body [post-body]
  (if-let [parsed-json (safe-parse-json post-body)]
    (if (validate-parsed-json parsed-json)
      parsed-json)))

(defn post-new-number [body-stream store]
  "Add a new record to the store by:
   1. Reading the body stream
   2. Parsing it in to a CallerIDRecord
   3. swap! it like it's hot
   4. Return a 201 on a successful create, or a 400 if the POST body was malformed"
  (let [body-text (slurp body-stream)]
    ;; Is the JSON sufficiently OK?
    (if-let [parsed-json (safe-parse-and-validate-post-body body-text)]
      ;; Yes, it is!
      (let [new-record (map->CallerIDRecord parsed-json)
            root-uri   (get-root-uri)
            loc        (str root-uri "/query?number=" (url-encode (:number new-record)))]
        (log/debug "Upserting" new-record)
        (swap! store upsert new-record)
        (respond-http-post-success body-text loc))
      ;; It is totally not OK!
      (do
        (log/debug "Rejecting malformed POST body:" body-text)
        (four-hundred "Malformed JSON in POST body" 400)))))

(defn init! []
  "Bootstrap the API"
  (log/debug "Bootstrapping...")
  (try
    (let [seed-data (load-and-parse-csv seed-file-name)]
      (build-lookup seed-data application-storage)
      (log/debug "Bootstrapping complete"))
    (catch Exception e
      (log/error "Bootstrapping failed! Aborting application start!")
      (log/error "This is most likely caused by missing seed data; be sure the file"
                 seed-file-name
                 "exists under resources/ and is formatted correctly.")
      (System/exit 1))))

(defroutes application-routes
  (GET "/query" [number] (query number application-storage))
  (ANY "/query" [] (four-hundred "Endpoint /query only supports GET" 405))
  (POST "/number" [:as {body :body}] (post-new-number body application-storage))
  (ANY "/number" [] (four-hundred "Endpoint /number only supports POST" 405))
  (ANY "*" [] (four-hundred "Resource not found")))

(def application
  (-> (wrap-defaults application-routes api-defaults)
      wrap-reload))

(def cli-args
  [["-p" "--port PORT" "Port to bind to"
    :default 8080
    :parse-fn #(Integer/parseInt %)
    :validate [#(< 0 % 0x10000) "Must be a number between 0 and 65536"]]
   [nil "--ip IP" "IP address to bind to"]])

(defn parse-cli-args [args]
  (let [opts (parse-opts args cli-args)]
    (if-let [err (:errors opts)]
      (do
        (log/error "Error in arguments!")
        (log/error err)
        (System/exit 1))
      opts)))

;; Graceful shutdown machinery
(defn shutdown-gracefully []
  (let [server (:server @service)]
    (when-not (nil? server)
      (log/info "Received shutdown signal! Shutting down...")
      (server 100)))
  (swap! service assoc :server nil))

(defn add-shutdown-hook []
  (let [runtime (Runtime/getRuntime)
        shutodwn-op (Thread. shutdown-gracefully)]
    (.addShutdownHook runtime shutodwn-op)))

(defn -main
  "I don't do a whole lot ... yet."
  [& args]
  (let [parsed-args (parse-cli-args args)
        opts        (:options parsed-args)]
    (init!)
    (reset! service
            {:server (http-kit/run-server application opts)
             :opts   opts})
    (log/info "Server up on localhost! Started with args:" opts)))
