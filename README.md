# bimblebomble

Stunt programming at its finest.

`bimblebomble` implements a basic caller ID lookup service. It exposes the following endpoints:

### GET /query

Given a phone number in E.164 format, return a list of caller ID records. Note
that phone numbers are not unique in this API, and several callers may be
returned.

Query Params:
number - the number we want caller id information for.

API Response:
```json
{results: [{ “name”: “Bob Barker”, “number”: “+15556789090”, “context”: “personal”}]}
```

### POST /number

Add a new caller ID record to the service; expects a single JSON object as the body:

Body:
name - contact name
number - the number in E.164 format
context - the context for the phone number.

## Design Considerations

Design thoughts and dev notes have been written up in the file
[notes.org](notes.org), which is best read in Emacs org-mode (Gitlab can parse
it, but the results are... a little odd).

## Requirements
`bimblebomble` requires leiningen to build; installation instructions can be
found [here](https://leiningen.org/). `bimblebomble` was built and tested
against lein 2.7.1. Any 2.x lein _should_ work, but your mileage may vary.

`bimblebomble`, and lein for that matter, require a Java installation. The app
was built and tested against Java 1.8; I would not advise running an older Java
version. (At all -- 1.7 is several years out of security updates.)

`bimblebomble` has no OS-specific requirements; it _should_ work on any platform
with a current JVM. That said: it was tested on macOS Sierra 10.12.6.

## Setup

`bimblebomble` has exactly one setup step: correctly formatted seed data needs
to be present under `resources/`. "Correctly formatted" means an uncompressed
CSV file, with no headers and unquoted fields, in which each row is a triple of
`phone number, context, caller name`.

**Note** that `bimblebomble` can correctly handle phone numbers in the CSV in
_either_ E.164 format (preferred) or the more typical American `(NNN) NNN-NNNN`
format.

## Running the service

`bimblebomble` can be run in two ways:

- `lein run`             -- great for testing
- Compiled as an uberjar -- much better for deploy

To build and run the uberjar:

```bash
lein uberjar
java -jar target/uberjar/bimblebomble-0.1.0-SNAPSHOT-standalone.jar
```

`bimblebomble` accepts two optional command-line arguments:

- `-p/--port <INT>` -- specify the port to bind to
- `--ip <IP>`       -- specify the IP address to bind to

### Running the test suite

`bimblebomble` has a reasonably complete test suite. Run it with `lein test`.

## License

Copyright © 2017 Ross M. Donaldson

Distributed under the Eclipse Public License either version 1.0 or (at
your option) any later version.
