(defproject bimblebomble "0.1.0-SNAPSHOT"
  :description "You asked for it ;-P"

  :url "https://csv.rodeo" ;; Just my personal website, thx.

  :scm {:name "git"
        :url "git@gitlab.com:gastove/bimblebomble.git"}

  :license {:name "Eclipse Public License"
            :url "http://www.eclipse.org/legal/epl-v10.html"}

  :dependencies [[org.clojure/clojure "1.8.0"]
                 [compojure "1.6.0"]
                 [http-kit "2.2.0"]
                 [ring/ring-defaults "0.3.1"]
                 [ring/ring-devel "1.6.2"]
                 [cheshire "5.8.0"]
                 [com.taoensso/timbre "4.10.0"]
                 [org.clojure/tools.cli "0.3.5"]
                 [ring/ring-json "0.4.0"]]

  :main ^:skip-aot bimblebomble.core

  :target-path "target/%s"

  :profiles {:uberjar {:aot :all}})
